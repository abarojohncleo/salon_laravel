<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentModesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_modes')->insert(['name'=>'Stripe']);
        DB::table('payment_modes')->insert(['name'=>'Paypal']);
        DB::table('payment_modes')->insert(['name'=>'Cash On Delivery']);
     
    }
}