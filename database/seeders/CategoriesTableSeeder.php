<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(['name'=>'Facial Product']);
        DB::table('categories')->insert(['name'=>'Skin Product']);
        DB::table('categories')->insert(['name'=>'Hair Product']);
        DB::table('categories')->insert(['name'=>'Maintenance Product']);
    }
}