<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\User;
use App\Notifications\TransactionPaid;

class CartController extends Controller
{
    public function add(Request $request) 
    {
    	$item_id = $request->item_id;
    	$quantity = intval($request->quantity);

    	$request->session()->flash('message', 'Item has been added.');

    	if ($request->session()->exists('cart.'.$item_id)) {
    		$currentQuantity = $request->session()->get('cart.'.$item_id);
    		$request->session()->put('cart.'.$item_id, $quantity + $currentQuantity);
    	} else {
    		$request->session()->put('cart.'.$item_id, $quantity);
    	}
    }


    public function update(Request $request) 
    {
        $item_id = $request->item_id;
        $quantity = $request->quantity;
        $request->session()->put('cart.'.$item_id, $quantity);
        return redirect('/cart');   
    }

    public function remove(Request $request) 
    {
        $item_id = $request->item_id;
        $request->session()->forget('cart.'.$item_id);
        return redirect('/cart');
    }

    public function empty(Request $request) 
    {
        $request->session()->forget('cart');
        return redirect('/cart');
    }

    public function index(Request $request) 
    {
        $cart = $request->session()->get('cart');
        $item_ids = (!empty($cart)) ? array_keys($cart): [];
        $items = Item::find($item_ids);

        return view('cart', [
            'items' => $items
        ]);
    }

    public function checkout(Request $request, $type) 
    {
        if($type == 'cash') {
            $order = new Order;
    		$cart = $request->session()->get('cart');
    		$item_ids = array_keys($cart);
    		$items = Item::find($item_ids);
    		$order->user_id = Auth::user()->id;
    		$order->payment_mode_id = 2;
    		$order->created_at = now();
			$order->save();
			
			User::find($order->user_id)->notify(new TransactionPaid);

    		foreach ($items as $item) {
    			$order_item = new OrderItem;

    			$order_item->order_id = $order->id;
    			$order_item->item_id = $item->id;
    			$order_item->quantity = $request->session()->get('cart.'.$item->id);
    			$order_item->unit_price = $item->unit_price;

    			$order_item->save();
    		}

        } elseif ($type == 'paypal'){
            $order = new Order;
    		$cart = $request->session()->get('cart');
    		$item_ids = array_keys($cart);
    		$items = Item::find($item_ids);
    		$order->user_id = Auth::user()->id;
    		$order->payment_mode_id = 3;
    		$order->created_at = now();
			$order->save();
			
			User::find($order->user_id)->notify(new TransactionPaid);

    		foreach ($items as $item) {
    			$order_item = new OrderItem;

    			$order_item->order_id = $order->id;
    			$order_item->item_id = $item->id;
    			$order_item->quantity = $request->session()->get('cart.'.$item->id);
    			$order_item->unit_price = $item->unit_price;

    			$order_item->save();
    		}

        } 

        $request->session()->forget('cart');
        return redirect('/user-transactions');
    }
}
