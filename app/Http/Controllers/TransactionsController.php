<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;

class TransactionsController extends Controller
{
    public function index()
    {
        $orders = Order::all();
        return view('transactions', [
            'orders' => $orders,
        ]);
    }
}
