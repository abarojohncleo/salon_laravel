<?php
   
namespace App\Http\Controllers;
   
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\User;
use App\Notifications\TransactionPaid;
use Session;

use Stripe;

   

class StripePaymentController extends Controller

{

    /**

     * success response method.

     *

     * @return \Illuminate\Http\Response

     */

    public function stripe(Request $request)

    {
        $cart = $request->session()->get('cart');
        $item_ids = (!empty($cart)) ? array_keys($cart): [];
        $items = Item::find($item_ids);

        return view('stripe', [
            'items' => $items
        ]);
    }
    /**

     * success response method.
     *
     * @return \Illuminate\Http\Response
     */

    public function stripePost(Request $request)

    {
            $order = new Order;
    		$cart = $request->session()->get('cart');
    		$item_ids = array_keys($cart);
    		$items = Item::find($item_ids);
    		$order->user_id = Auth::user()->id;
    		$order->payment_mode_id = 2;
    		$order->created_at = now();
    		$order->save();

    		foreach ($items as $item) {
    			$order_item = new OrderItem;

    			$order_item->order_id = $order->id;
    			$order_item->item_id = $item->id;
    			$order_item->quantity = $request->session()->get('cart.'.$item->id);
                $order_item->unit_price = $item->unit_price;
                $subtotal =$item->unit_price * session('cart.'.$item->id);
                $total = $subtotal;

                User::find($order->user_id )->notify(new TransactionPaid);

                $order_item->save();
                
                Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                Stripe\Charge::create ([

                    "amount" => $total * 100,
                    "currency" => "PHP",
                    "source" => $request->stripeToken,
                    "description" => "Test by johnny." 
                ]);
    		}
        $request->session()->forget('cart');
        return redirect('/user-transactions');

    }

}