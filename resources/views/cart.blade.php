@extends('layouts.app')

@section('title', 'Cart')

@section('cart-rows')

	@php
		$total  = 0;
	@endphp

	@foreach ($items as $item)

		@php
			$subtotal =$item->unit_price * session('cart.'.$item->id);
			$total += $subtotal;
		@endphp

		<tr>
			<td>{{ $item->name }}</td>
			<td class='text-right'>{{ number_format($item->unit_price, 2) }}</td>
			<td>
				<form method='post' action='/cart/update'>
					@csrf
					@method('PUT')

					<input type="hidden" name="item_id" value="{{ $item->id }}">

					<div class="btn-group btn-block">
						<input type="number" name="quantity" value='{{ session("cart.".$item->id) }}' class="form-control" required>
						<button class="btn btn-success">Update</button>
					</div>

				</form>
			</td>
			<td class="text-right">{{ number_format($subtotal, 2) }}</td>
			<td>
				<form method="post" action="/cart/remove">
					@csrf
					@method('DELETE')
					<input type="hidden" name="item_id" value="{{ $item->id }}">
					<button class="btn btn-danger">Remove</button>
				</form>
			</td>
		</tr>
	@endforeach

@endsection

@section('content')
	
	<div class="container-fluid">
		@if (!empty(session('cart')))

			<h3>Cart</h3>

			<table class="table table-bordered">
				<thead>
					
					<tr>
						<th>Item</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Subtotal</th>
						<th>Action</th>
					</tr>

				</thead>

				<tbody>
					@yield('cart-rows')

					<tr>
						<td colspan="3" class="text-right"><b>Total</b></td>
						<td class="text-right">{{ number_format($total, 2) }}</td>
						<td></td>
					</tr>

				</tbody>
			</table>
			<div>
				<form action="/cart/empty" action="d-inline" method="post">
					@csrf
					@method('DELETE')
					<button class="btn btn-danger">Empty Cart</button>
				</form>
				<br>
				<form action="/cart/checkout/cash" action="d-inline" method="post">
					@csrf
					@method('POST')
					<button class="btn btn-primary">Proceed to checkout</button>
				</form>

				<a class="btn btn-primary" href="{{url('/stripe')}}">Stripe Payment</a>
				<br>

				<form action="/cart/checkout/paypal" hidden id="form-checkout-paypal" method="post">
					@csrf
					@method('POST')
				</form>

				<div id="paypal-button-container" class="btn mt-auto"></div>
                

			</div>

			@else

			<h3>No items in cart.</h3>

			<h4>Select a menu item to add to cart from. <a href="{{ url('/lists') }}">here</a></h4>

			@endif
	</div>

	<script>

		const btnPaypalStyle = {
			height: 38,
			shape: 'rect',
			layout: 'horizontal'
		};
		paypal.Buttons({
		 style: btnPaypalStyle,
		 createOrder: function(data, actions) {
		 	//set up transaction details including amount and item details.
		 	return actions.order.create({
		 		purchase_units: [{
		 			amount: {
		 				value: '{{ $total }}'
		 			}
		 		}]
		 	})
		 },
		 onApprove: function(data,actions) {
		 	return actions.order.capture().then(function(details) {
		 		//this function shows transaction success message to your buyer.
		 		alert('Transaction completed by ' + details.payer.name.given_name);
		 		document.querySelector('#form-checkout-paypal').submit();
		 	})
		 }
		  }).render('#paypal-button-container');

	</script>

@endsection
