@if(Auth::user()->user_role != 'admin')
	<script type="text/javascript">window.location= '/lists'</script>
@endif

@extends('layouts.app')

@section('title', 'Add Item')

@section('add-item-form')
    <form action="{{ url('items/store') }}" method="POST" enctype="multipart/form-data">
        
        @csrf
        <div class="form-group">
            <label>Item Name</label>
            <input type="text" class="form-control" name="name" required>
        </div>

        <div class="form-group">
            <label>Description</label>
            <input type="text" class="form-control" name="description">
        </div>

        <div class="form-group">
            <label>Price</label>
            <input type="number" class="form-control" name="unit_price" required>
        </div>

        <div class="form-group">
            <label>Category</label>
            <select name="category_id" class="form-control">
                <option value selected disabled>Select Category </option>
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label>Image</label>
            <input type="file" class="form-control" name="image" required>
        </div>

        <button type="submit" class="btn btn-success btn-block">Add</button>

    </form>

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-6 mx-auto">
                <h3 class="text-center">Add Item</h3>
                <div class="card">
                    <div class="card-header">Item Informatio</div>
                    <div class="card-body">
                        @yield('add-item-form')
                    </div>
                </div>
            </div> 
        </div>
    </div>
@endsection