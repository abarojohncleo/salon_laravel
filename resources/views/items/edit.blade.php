@if(Auth::user()->user_role != 'admin')
	<script type="text/javascript">window.location= '/lists'</script>
@endif

@extends('layouts.app')

@section('title', 'Edit Item')

@section('edit-item-form')

	<form action='{{ url("items/$item->id") }}' method="post" enctype="multipart/form-data">
		
		@csrf

		@method("PUT")

		<div class="form-group">
			<label>Item Name</label>
			<input type="text" name="name" class="form-control" value="{{ $item->name }}" required>
		</div>

		<div class="form-group">
			<label>Description</label>
			<input type="text" name="description" class="form-control" value="{{ $item->description }}">
		</div>

		<div class="form-group">
			<label>Price</label>
			<input type="number" name="unit_price" class="form-control" value="{{ $item->unit_price }}" required>
		</div>

		<div class="form-group">
			<label>Category</label>
			<select class="form-control" name="category_id">
				<option value selected disabled>Select Category</option>
				@foreach ($categories as $category)
					@if ($category->id == $item->category_id)
						<option value="{{ $category->id }}" selected>{{ $category->name }}</option>
					@else
						<option value="{{ $category->id }}">{{ $category->name }}</option>
					@endif
				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label>Image</label>
			<input type="file" name="image" class="form-control" required>
		</div>

		<button type="submit" class="btn btn-success btn-block">Update</button>

	</form>

@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-6 mx-auto">
				<h3 class="text-center">Edit Item</h3>
				<div class="card">
					<div class="card-header">Item Information</div>
					<div class="card-body">
						@yield('edit-item-form')
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
