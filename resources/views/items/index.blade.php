@extends('layouts.app')

@section('title', 'Store Page')

@section('content')
    <div class="container-fluid">
        <h3>Store</h3>

        @if (!empty(Auth::user()) && Auth::user()->user_role == 'admin')
            <a href="{{url('items/create')}}" class="btn btn-primary">Add Item</a>
        @endif

        <div class="row container-fluid">
        @foreach ($items as $item)
            <div class="card m-2" style="width: 18rem;">
                <img class="card-img-top" src='{{ asset("storage/$item->image_location") }}' alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">{{ $item->name }}</h5>
                    <p class="card-text">{{ $item->description }}</p>
                    <p class="card-text">&#8369; {{ $item->unit_price }}</p>
                    @if (!empty(Auth::user()))

                        @if(Auth::user()->user_role == "admin")

                            <div class="btn-group btn-block">
                                <a href='{{ url("items/$item->id/edit") }}' class="btn btn-outline-success">Edit</a> 
                                <a href='{{ url("items/$item->id/delete-confirm") }}' class="btn btn-outline-danger">Delete</a>
                            </div>

                        @elseif (Auth::user()->user_role == "customer")

                            <form class="form-add-to-cart" data-id="{{ $item->id }}" method="post">
                                <div class="btn-group btn-block">
                                    <input type="number" class="form-control" name="quantity" value="1" min="0">
                                </div>
                                <button class="btn btn-success btn-block">Add</button>
                            </form>

                        @endif
                    @endif
                </div>
            </div>
        @endforeach
        </div>
    </div>

    @if (!empty(Auth::user()))
		@if (Auth::user()->user_role == 'customer')
			<script type="text/javascript">
				const formsAddToCart = document.querySelectorAll('.form-add-to-cart');
				const spanCartCount = document.querySelector('#span-cart-count');

				formsAddToCart.forEach(function(formAddToCart) {
					formAddToCart.addEventListener('submit', addToCart);
				});

				function addToCart(event) {
					event.preventDefault();

					const controllerUrl = '/cart/add' ;
					const itemId = event.target.getAttribute('data-id');
					const quantity = event.target.quantity.value;

					let formData = new FormData();
					formData.append('item_id', itemId);
					formData.append('quantity', quantity);
					formData.append('_token', "{{ csrf_token() }}");

					let payload = {
						method: 'post',
						body: formData
					};

					fetch(controllerUrl, payload).then(function(response) {
						return response.text();
					}).then(function(response) {
						spanCartCount.innerHTML = parseInt(spanCartCount.innerHTML) + parseInt(quantity);
					});
				}
 			</script>
 		@endif
 	@endif

@endsection

@if(!empty(session()->get('message')))
    <script>alert('{{ session()->get("message") }}')</script>
@endif