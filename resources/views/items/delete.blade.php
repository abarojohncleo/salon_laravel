@if(Auth::user()->user_role != 'admin')
	<script type="text/javascript">window.location= '/lists'</script>
@endif

@extends('layouts.app')

@section('title', 'Delete Form')

@section('delete-item-form')

	<form action='{{ url("items/$item->id") }}' method="post" enctype="multipart/form-data">

		@csrf

		@method("DELETE")

		<div class="form-group">
			<label>Item Name</label>
			<input type="text" class="form-control" value="{{ $item->name }}" readonly>
		</div>

		<div class="form-group">
			<label>Description</label>
			<input type="text" class="form-control" value="{{ $item->description }}" readonly>
		</div>

		<div class="form-group">
			<label>Price</label>
			<input type="number" class="form-control" value="{{ $item->unit_price }}" readonly>
		</div>

		<div class="form-group">
			<label>Category</label>
			<input type="text" class="form-control" value="{{ $category->name }}" readonly>
		</div>

		<button type="submit" class="btn btn-danger btn-block">Delete</button>

	</form>

@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-6 mx-auto">
				<h3 class="text-center">Delete Item</h3>
				<div class="card">
					<div class="card-header">Item Information</div>
					<div class="card-body">
						@yield('delete-item-form')
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

