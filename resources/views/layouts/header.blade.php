@php
    $cart_quantity;
    $cart = session('cart');

    if (!empty($cart)) {
        $cart_quantity = array_sum ($cart);
    } else {
        $cart_quantity= 0;
    }
@endphp

<nav class="navbar navbar-expand-lg navbar-dark bg-success">
    <a href="" class="navbar-brand">USalon</a>

    <button class="navbar-toggler navbar-toggler-right " type="button" data-toggle="collapse" data-target="#navbar">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse text-right" id="navbar">

        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a href="{{url('/lists')}}" class="nav-link">Store</a>
            </li>
            @if (!empty(Auth::user()) && Auth::user()->user_role == 'customer')
                <li class="nav-item">
                    <a href="{{url('/cart')}}" class="nav-link">Cart
                        <span id="span-cart-count" class="badge badge-light">{{ $cart_quantity }}</span>
                    </a>
                </li>
            @endif
        </ul>

        <ul class="navbar-nav ml-auto">
            @guest
                <li class="nav-item">
                    <a href="{{url('/login')}}" class="nav-link">Login</a>
                </li>

                <li class="nav-item">
                    <a href="{{url('/register')}}" class="nav-link">Register</a>
                </li>
            @else
                @if (!empty(Auth::user()) && Auth::user()->user_role == 'customer')
                    <li class="nav-item">
                        <a href="{{url('/user-transactions')}}" class="nav-link">Transactions</a>
                    </li>
                @else
                    <li class="nav-item">
                        <a href="{{url('/transactions')}}" class="nav-link">Transactions</a>
                    </li>
                @endif

                <li class="nav-item">
                    <a onclick="document.querySelector('#logout-form').submit()" href="#" class="nav-link">Logout</a>
                </li>
            @endguest
        </ul>
    </div>   
</nav>

<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
    @csrf
</form>