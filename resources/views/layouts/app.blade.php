<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>

    <script src="https://www.paypal.com/sdk/js?client-id=AXBZIYDIaeOtY4fQpbiS4OGqZx_TgUG9ggrhLr5P_LuP2Ece1dPn8U51VpKkrfOmK_3RH3evhANLzg5T&currency=PHP"></script>
    @include('layouts.header')
    <main class="py-4">
        @yield('content')
    </main>
    @include('layouts.footer')
    
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" defer></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" defer></script>
</body>
</html>