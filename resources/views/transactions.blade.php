@extends('layouts.app')

@section('title', 'Transaction Page')

@section('transactions')
	<table class="table table-striped table-dark text-center">
		<thead>
			<tr>
				<th scope="col">Item</th>
				<th scope="col">Price</th>
				<th scope="col">Qunatity</th>
				<th scope="col">Subtotal</th>
				<th scope="col">Total</th>
			</tr>
		</thead>
		<tbody>

			@foreach ($orders as $order)
				@php
					$total = 0;
				@endphp
				@foreach ($order->items as $order_item)
					@php 
						$subtotal = $order_item->unit_price * $order_item->quantity;
						$total += $subtotal;
					@endphp
					<tr>
						<td>{{$order_item->item->name}}</td>
						<td>{{$order_item->unit_price}}</td>
						<td>{{$order_item->quantity}}</td>
						<td>{{number_format($subtotal, 2)}}</td>
				@endforeach	
						<td>Php{{ number_format($total, 2) }}</td>
					</tr>
				
			@endforeach

		</tbody>
	</table>
@endsection

@section('content')
	<div class="container-fluid">
		<h3>Transactions</h3>
		<div>
			@yield('transactions')
		</div>
	</div>
@endsection