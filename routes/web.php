<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\TransacionsController;
use App\Models\Order;
use App\Models\OrderItem;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/lists', [App\Http\Controllers\ItemController::class, 'index'])->name('store');

Route::get('/items/create', [App\Http\Controllers\ItemController::class, 'create']);
Route::post('/items/store', [App\Http\Controllers\ItemController::class, 'store']);
Route::get('/items/{id}/edit', [App\Http\Controllers\ItemController::class, 'edit']);
Route::put('/items/{id}', [App\Http\Controllers\ItemController::class, 'update']);
Route::get('/items/{id}/delete-confirm', [App\Http\Controllers\ItemController::class, 'deleteConfirm']);
Route::delete('/items/{id}', [App\Http\Controllers\ItemController::class, 'destroy']);


Route::post('/cart/add', [App\Http\Controllers\CartController::class, 'add']);
Route::get('/cart', [App\Http\Controllers\CartController::class, 'index']);
Route::delete('/cart/empty', [App\Http\Controllers\CartController::class, 'empty']);
Route::delete('/cart/remove', [App\Http\Controllers\CartController::class, 'remove']);
Route::put('/cart/update', [App\Http\Controllers\CartController::class, 'update']);
Route::post('/cart/checkout/{type}', [App\Http\Controllers\CartController::class, 'checkout']);

Route::get('/transactions', [App\Http\Controllers\TransactionsController::class, 'index']);

Route::get('/user-transactions', function() {
	$orders = Order::where('user_id', Auth::user()->id)->get();
	return view('transactions', [
		'orders' => $orders
	]);
});

Route::get('stripe', [App\Http\Controllers\StripePaymentController::class, 'stripe']);
Route::post('stripe', [App\Http\Controllers\StripePaymentController::class, 'stripePost'])->name('stripe.post');;